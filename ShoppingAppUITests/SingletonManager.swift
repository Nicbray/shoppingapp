//
//  SingletonManager.swift
//  ShoppingApp
//
//  Created by Nicole Braybrook on 19/11/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//
import Foundation

class SingletonManager {
    static let model = Model()
}

//
//  Cell.swift
//  ShoppingApp
//
//  Created by Nicole Braybrook on 20/11/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//

import UIKit

class Cell: UICollectionViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    
    @IBOutlet weak var cellLabel: UILabel!
    
}

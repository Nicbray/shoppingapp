//
//  Product.swift
//  ShoppingApp
//
//  Created by Nicole Braybrook on 20/11/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//


import UIKit

class Product: NSObject {
    
    var name: String! = ""
    var price = 0.0
    var image: UIImage?
    var desc: String! = ""
    var category: String! = ""
    var uid: String! = ""
    var cart = false
    
    
    override init() {
    }
    
    init(name: String, price: Double, image: UIImage, description: String, category: String, cart: Bool, uid: String) {
        self.name = name
        self.price = price
        self.image = image
        self.desc = description
        self.category = category
        self.uid = uid
        self.cart = cart
    }
    
    init(name: String, price: Double, image: UIImage, description: String, category: String, uid: String) {
        self.name = name
        self.price = price
        self.image = image
        self.desc = description
        self.category = category
        self.uid = uid
    }
    
    init(name: String, price: Double, description: String, category: String, uid: String) {
        self.name = name
        self.price = price
        self.desc = description
        self.category = category
        self.uid = uid
    }
    
    
    override func isEqual(_ object: Any?) -> Bool {
        if let otherProduct = object as? Product {
            if self.uid == otherProduct.uid && self.name == otherProduct.name && self.image!.isEqual(otherProduct.image) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
    }
}

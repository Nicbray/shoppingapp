//
//  CheckoutController.swift
//  ShoppingApp
//
//  Created by Nicole Braybrook on 26/11/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//

import Foundation
import UIKit

class CheckoutViewController: DetailViewController {
    
    let model = SingletonManager.model

    @IBOutlet var painting: UISegmentedControl!
    @IBOutlet var printing: UISegmentedControl!
    
    @IBOutlet var ccn: UITextField!
    @IBOutlet var cce: UITextField!
    @IBOutlet var ccc: UITextField!
    
    @IBOutlet var result: UILabel!
    @IBOutlet var ccVerify: UILabel!

    @IBOutlet var items: UITextView!

    @IBAction func printingChanged(_ sender: Any) {
        self.writeUp()
    }
    @IBAction func paintingChanged(_ sender: Any) {
        self.writeUp()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.writeUp()
    }
    
    // ensure cc fields arent empty
    func verifyCC() -> Bool{
        if (ccn.text == "" || cce.text == "" || ccc.text == ""){
            ccVerify.text = "Please enter credit card details"
            return false
        } else {
            ccVerify.text = "CC verified"
            return true
        }
    }
    
    // gather details for json url
    func writeUp(){
        
    items.text = model.checkoutCartString()
        
    items.text = items.text +  "\n Total Price: $" + String(calculateTotal())
    }
    
    // calculate total based on usrs selection
    func calculateTotal() -> Double{
        var total = model.totalForCart()
        
        if (printing.selectedSegmentIndex == 0){
            // do nothing
        } else {
            // add 10 percent to total
            total = ((total * 0.1) + total)
        }
        
        if (painting.selectedSegmentIndex == 0){
            // do ntohing
        } else {
            //Painting should add 55 percent to the current total
            total = ((total * 0.55) + total)
            
        }
        
        return total
        
    }
    
    @IBAction func submit(_ sender: Any) {
        
        if (verifyCC()){
        let success = model.checkoutURL(total: calculateTotal(), material: printing.selectedSegmentIndex == 1, painting: painting.selectedSegmentIndex == 1)
        
        if (success == true) {
            result.text = "Payment was successful! \n Your order has been placed"
            
        } else {
            // uncsuccessful
            result.text = "Payment unsuccessful! Try again"

            }}
    }
    


}

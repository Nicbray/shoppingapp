//
//  LocationFinderController.swift
//  ShoppingApp
//
//  Created by Nicole Braybrook on 25/11/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class LocationFinderViewController: DetailViewController {
    
    @IBOutlet weak var map: MKMapView!

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var street: UITextField!

    @IBOutlet weak var suburb: UITextField!
    
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var addressConfirmation: UILabel!

    
    typealias FinishedDownload = () -> ()
    
    var address = ""
    
    let model = SingletonManager.model
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

    }
    
    
    func updateMap(close: Bool){
        
        let myGroup = DispatchGroup()
        
        
        myGroup.enter()
        
        model.getUsersCoordinates(address: address)
        myGroup.leave()

        myGroup.enter()
        
        model.getClosestStore()
        myGroup.leave()

        
            myGroup.enter()
            
            // zoom in on adelaide
            let coordinations = CLLocationCoordinate2D(latitude: model.closestStore.lat,longitude: model.closestStore.long)
        var span = MKCoordinateSpanMake(0.5,0.5)

        if (close == true){
            span = MKCoordinateSpanMake(0.008,0.008)
        }
            let region = MKCoordinateRegion(center: coordinations, span: span)
            
            map.setRegion(region, animated: true)
            
            
            myGroup.leave()

        
        myGroup.notify(queue: .main) {
            print("Finished all requests.")
        }

    }
    
    @IBAction func findUser(_ sender: Any) {
        
        address = street.text! + ", " + suburb.text! + ", Australia"
        
        model.getUsersCoordinates(address: address)
        
        addressConfirmation.text = "There are " + String(model.stores.count) + " stores around you."
        
        result.text = "Enter an address or leave blank for Adelaide CBD"
        
        updateMap(close: false)
        
    }

    @IBAction func findLocation(_ sender: Any) {

        address = street.text! + ", " + suburb.text! + ", Australia"
        
        model.getUsersCoordinates(address: address)
        
        addressConfirmation.text = "There are " + String(model.stores.count) + " stores around you."
        
        updateMap(close: true)
        
        result.text = model.closestStore.toString()

        
    }

}

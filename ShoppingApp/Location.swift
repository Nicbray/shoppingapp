//
//  Location.swift
//  ShoppingApp
//
//  Created by Nicole Braybrook on 25/11/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//

import Foundation
import CoreLocation

class Location: NSObject {
    
    // JSON values
    var street: String = ""
    var suburb: String = ""
    var postcode: Int = 0
    var state: String = ""
    var countrycode: String = ""
    var uid = ""
    var lat = 0.00
    var long = 0.00

    override init() {
    }

    // street": "939 Marion Rd", "suburb": "Mitchell Park", "postcode": "5043", "state": "SA", "countrycode": "au", "uid": "a0001", "coord1": "-35.0143878", "coord2"
    
    init(street: String, suburb: String, postcode: String, state: String, countrycode: String, uid: String, coord1: String, coord2: String){
        self.street = street
        self.suburb = suburb
        self.postcode = Int(postcode)!
        self.state = state
        self.countrycode = countrycode
        self.uid = uid
        self.lat = Double(coord1)!
        self.long = Double(coord2)!
    }
    
    init(lat: Double, long: Double){
        self.lat = lat
        self.long = long
    }
    
    func distanceInMeters(other: Location) -> Double{
        let currentLoc = CLLocation(latitude: lat, longitude: long)
        let otherLoc = CLLocation(latitude: other.lat, longitude: other.long)
        
        return currentLoc.distance(from: otherLoc)
    }
    
    func toString() -> String{
        return street + ", " + suburb + ", " + String(postcode) + ", " + state + ", " + countrycode + "."
    }

    override func isEqual(_ object: Any?) -> Bool {
        if let otherLocation = object as? Location {
            if self.uid == otherLocation.uid {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
    }
    
}

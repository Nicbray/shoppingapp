//
//  Model.swift
//  ShoppingApp
//
//  Created by Nicole Braybrook on 19/11/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//


import UIKit
import CoreData
import MapKit
import CoreLocation

class Model {
    
    var segueArray = [String]()
    var segueDictionary = Dictionary<String, UIImage>()
    
    var products = [Product]()
    var storedProducts = [NSManagedObject]()
    
    var searchedProducts = [Product]()
    
    var stores = [Location]()
    var usersLocation = Location()
    var closestStore = Location()
    
    let geocoder = CLGeocoder()
    
    
    init() {
        segueArray.append("Home")
        segueArray.append("Products")
        segueArray.append("Cart")
        segueArray.append("Search")
        segueArray.append("Location")
        segueArray.append("Checkout")
        
        segueDictionary["Home"] = UIImage(named: "home")
        segueDictionary["Products"] = UIImage(named: "menu")
        segueDictionary["Cart"] = UIImage(named: "menu")
        
        self.refreshProducts()
        self.loadProducts()
        self.getLocations()
    }
    
    var cart = [Product]()
    
    func refreshProducts()
    {
        
        let url = NSURL(string: "http://partiklezoo.com/3dprinting")
        let config = URLSessionConfiguration.default
        config.isDiscretionary = true
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: url! as URL, completionHandler:
        {(data, response, error) in
            do {
                let json = try JSON(data: data!)
                
                for count in 0...json.count - 1
                {
                    let newProduct = Product(name: json[count]["name"].string!, price: json[count]["price"].doubleValue, description: json[count]["description"].string!, category: json[count]["category"].string!, uid: json[count]["uid"].string!)
                    
                    
                    let imgURL = json[count]["image"].string!
                    
                    
                    self.addItemToProducts(newProduct, imageURL: imgURL)
                }
            }
            catch let error as NSError
            {
                print("Could not convert. \(error), \(error.userInfo)")
            }
            
        })
        task.resume()
    }
    
    func loadProducts() {
        print("inside load")
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Products")
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            
            storedProducts = results as! [NSManagedObject]
            
            if (storedProducts.count > 0)
            {
                for index in 0 ... storedProducts.count - 1
                {
                    
                    let name = storedProducts[index].value(forKey: "name") as! String
                    let price = storedProducts[index].value(forKey: "price") as! NSNumber.FloatLiteralType
                    let binaryData = storedProducts[index].value(forKey: "image") as! Data
                    let image = UIImage(data: binaryData)
                    let desc = storedProducts[index].value(forKey: "description") as! String
                    //let category = storedProducts[index].value(forKey: "category") as! String
                    let uid = storedProducts[index].value(forKey: "uid") as! String
                    
                    let loadedProduct = Product(name: name, price: price, image: image!, description: desc, category: "oi", uid: uid)
                    
                    products.append(loadedProduct)
                }
            }
        }
        catch let error as NSError
        {
            print("Could not load. \(error), \(error.userInfo)")
        }
    }
    
    func checkForProduct(_ searchItem: Product) -> Int {
        var targetIndex = -1
        
        if (products.count > 0) {
            for index in 0 ... products.count - 1 {
                if (products[index].uid.isEqual(searchItem.uid)) {
                    targetIndex = index
                    products[index].price = searchItem.price
                    products[index].category = searchItem.category
                    products[index].desc = searchItem.desc
                }
            }
        }
        
        return targetIndex
    }
    
    func addItemToProducts(_ newProduct: Product!, imageURL: String) {
        if (checkForProduct(newProduct) == -1)
        {
            let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            
            let picture = UIImageJPEGRepresentation(loadImage(imageURL), 1)
            
            let entity =  NSEntityDescription.entity(forEntityName: "Products", in:managedContext)
            
            let productToAdd = NSManagedObject(entity: entity!, insertInto: managedContext)
            
            productToAdd.setValue(picture, forKey: "image")
            productToAdd.setValue(newProduct.name, forKey: "name")
            productToAdd.setValue(newProduct.desc, forKey: "desc")
            productToAdd.setValue(newProduct.price, forKey: "price")
            productToAdd.setValue(newProduct.uid, forKey: "uid")
            newProduct.cart = true
            
            do
            {
                try managedContext.save()
            }
            catch let error as NSError
            {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
            storedProducts.append(productToAdd)
            products.append(newProduct)
        }
    }
    
    func refreshLocations(){
        
    }
    
    func loadImage(_ imageURL: String) -> UIImage
    {
        
        var image: UIImage!
        
        if let url = NSURL(string: imageURL) {
            if let data = NSData(contentsOf: url as URL){
                image = UIImage(data: data as Data)
            }
        }
        
        return image!
    }
    
    func updateProduct(_ newProduct: Product!) {
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Products")
        fetchRequest.predicate = NSPredicate(format: "uid = %@", newProduct.uid)
        
        do {
            if let fetchResults = try managedContext.fetch(fetchRequest) as? [NSManagedObject] {
                
                if fetchResults.count != 0 {
                    
                    let managedObject = fetchResults[0]
                    
                    managedObject.setValue(newProduct.desc, forKey: "desc")
                    do
                    {
                        try managedContext.save()
                    }
                    catch let error as NSError
                    {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
            }
        }
        catch let error as NSError
        {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func searchFor(searchTerm: String) -> Bool{
        
        var foundProducts = [Product]()
        
        for item in products{
            let productName = item.name.lowercased()
            let productuid = item.uid.lowercased()
            
            if ( (productName.contains(searchTerm)) || (productuid.contains(searchTerm)) ){
                foundProducts.append(item)
            }
        }
        
        if (foundProducts.count > 0){
            searchedProducts = foundProducts
            return true
        } else {
            searchedProducts = products
            return false
        }
    }
    
    func total() -> Double {
        
        var total = 0.0
        for item in cart{
            total = total + item.price
        }
        return total
    }
    
    // gets the users locations from their address
    func getUsersCoordinates(address: String){
        
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks: [CLPlacemark]?, error: Error?) -> Void in
            let placemark = placemarks?[0]
            
            self.usersLocation.lat = Double((placemark?.location!.coordinate.latitude)!)
            self.usersLocation.long = Double((placemark?.location!.coordinate.longitude)!)
            
        })
        
    }
    
    // retreives all locations
    func getLocations(){

        let url = NSURL(string: "http://partiklezoo.com/3dprinting/?action=locations&coord1=0.00&coord2=0.00")
        
        let config = URLSessionConfiguration.default
        config.isDiscretionary = true
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: url! as URL, completionHandler:
        {(data, response, error) in
            do {
                let json = try JSON(data: data!)
                
                for count in 0...json.count - 1
                {

                    let newLocation = Location(street: json[count]["street"].string!, suburb: json[count]["suburb"].string!, postcode: json[count]["postcode"].string!, state: json[count]["state"].string!, countrycode: json[count]["countrycode"].string!, uid: json[count]["uid"].string!, coord1: json[count]["coord1"].string!, coord2: json[count]["coord2"].string!)
                    
                    if (!self.stores.contains(newLocation)){
                        self.stores.append(newLocation)}
                }
            }
            catch let error as NSError
            {
                print("Could not convert. \(error), \(error.userInfo)")
            }
            
        })
        
        task.resume()
        
    }
    
    // returns the store which is the cloest to the users, address, in euclidean
    func getClosestStore(){
        
        var shortestDistance = Double.infinity
        var location = Location()
        
        for store in stores {
            print("looking at store " + store.street)
            if (usersLocation.distanceInMeters(other: store) < shortestDistance){
                print(store.street + " is " + String(usersLocation.distanceInMeters(other: store)) + " meters away")
                location = store
                shortestDistance = usersLocation.distanceInMeters(other: store)
            }
        }
        
        closestStore = location
        
    }
    
    // adds a new item to cart
    func addItemToCart(item:Product){
        let newItem = Product(name: item.name, price: item.price, image: item.image!, description: item.desc, category: item.category, cart: true, uid: item.uid)
        cart.append(newItem)
    }

    // removes an item from the cart
    func removeProductFromCart(item: Product) -> Bool{
        
        var index = 0
        for product in cart {
            if (product.uid == item.uid){
                cart.remove(at: index)
                return true
            }
            index += 1;
        }
        
        return false
    }
    
    func numberInCart(item: Product) -> Int {
        
        var count = 0
        
        for product in cart {
            if (product.uid == item.uid){
                count += 1
            }
        }
        return count
    }
    
    func getTotalForItem(item: Product) -> Double {
        var total = 0.0
        for product in cart {
            if (product.uid == item.uid){
                total += product.price
            }
        }
        
        return total
    }
    
    func totalForCart() -> Double{
        var total = 0.0
        
        var calculated = [String]()
        
        for item in cart {
            
            if (!calculated.contains(item.uid)){
                total += getTotalForItem(item: item)
                calculated.append(item.uid)
            }

        }
        return total
    }

    func checkoutCartString() -> String{
        
        var str = ""
        
        var calculated = [String]()
        
        for item in cart {
            
            if (!calculated.contains(item.uid)){
            str += String(numberInCart(item: item)) + " " + item.name + "(s) = $" + String(getTotalForItem(item: item)) + "\n"
            calculated.append(item.uid)
        }
            
        }
    return str
    }
    
    
    // returns true if it was successful
    func checkoutURL(total: Double, material: Bool, painting: Bool) -> Bool{
        
        // u0001=1&total=102.3&material=abs&painting=true
        
        var url = "http://partiklezoo.com/3dprinting/?action=purchase&"
        
        var calculated = [String]()
        
        for item in cart {
            if (!calculated.contains(item.uid)){
                url += item.uid + "=" + String(numberInCart(item: item)) + "&"
            calculated.append(item.uid)
            }
        }
        
        url += "total=" + String(total) + "&"
        
        if(material == true){
            url += "material=abs&"
        } else {
            url += "material=pla&"
        }
        
        if(painting == true){
            url += "painting=true"
        } else {
            url += "painting=false"
        }
        
        return checkout(strUrl: url)
    }
    
    
    // retreives successful checout or not
    func checkout(strUrl: String) -> Bool{
        
        let url = NSURL(string: strUrl)
        
        var successful = true;
        

        
        let config = URLSessionConfiguration.default
        config.isDiscretionary = true
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: url! as URL, completionHandler:
        {(data, response, error) in
            do {
                let json = try JSON(data: data!)
                
                for count in 0...json.count - 1
                {
                    
                    let success = json[count]["success"].boolValue
                    
                    
                }
            }
            catch let error as NSError
            {
                print("Could not convert. \(error), \(error.userInfo)")
            }
            
        })
        
        task.resume()
        
        return successful;
    }
}

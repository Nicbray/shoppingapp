//
//  ProductViewController.swift
//  ShoppingApp
//
//  Created by Nicole Braybrook on 20/11/17.
//  Copyright © 2017 Nicole Braybrook. All rights reserved.
//


import UIKit

class ProductViewController: DetailViewController {
    
    let model = SingletonManager.model
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var catLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var addToCartButton: UIButton!
    

    @IBOutlet weak var number: UILabel!
    
    var productItem: Product? {
        didSet {
            // Update the view.
        }
    }
    
    func setAddToCartButton() {
        addToCartButton.setTitle("Add to Cart", for: UIControlState())
    }
    
    override func configureView() {
        // Update the user interface for the detail item.
        if let product = self.productItem {
            self.productImage.image = product.image
            self.titleLabel.text = product.name
            self.catLabel.text = "Category: " + product.category
            self.descLabel.text = product.desc
            self.priceLabel.text = "$" + String(product.price)
            self.setAddToCartButton()
            self.number.text = String(model.numberInCart(item: product))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addToCartSelected(_ sender: AnyObject) {

        // append a copy of this item
        self.model.addItemToCart(item: self.productItem!)
        number.text = String(Int(number.text!)! + 1)
        
    }
    
    @IBAction func remove(_ sender: Any) {
        if (Int(number.text!)! != 0){
        number.text = String(Int(number.text!)! - 1)
        self.model.removeProductFromCart(item: self.productItem!)
        }
    }
}
